import { Plant } from "@/types/plantType";
import PlantListItem from "@/components/plantListItem";
import { useState } from "react";
import PlantForm from "./plantForm";

export default function PlantList({ plants }: { plants: Plant[] }) {

    const [items, setItems] = useState(plants);

    return (
        <>
            <PlantForm
                plant={{ id: null, name: '', slug: '', description: '', zone: '', created_at: '', seed_image: '' }}
                setItems={setItems}
                buttonText="Create" />
            <table className="shadow-lg bg-white">
            <tbody>
                <tr>
                    <th className="bg-blue-400 border text-center py-4">
                    ID
                    </th>
                    <th className="bg-blue-400 border text-center py-4">
                    Name
                    </th>
                    <th className="bg-blue-400 border text-center py-4">
                    Slug
                    </th>
                    <th className="bg-blue-400 border text-center py-4">
                    Description
                    </th>
                    <th className="bg-blue-400 border text-center py-4">
                    Zone
                    </th>
                    <th className="bg-blue-400 border text-center py-4">
                    Picture
                    </th>
                    <th className="bg-blue-400 border text-center py-4">
                    Created At
                    </th>

                    <th className="bg-blue-400 border text-center py-4">
                    Action
                    </th>
                </tr>
                {items && Array.isArray(items) &&
                    items.map((plant, index) => (
                        <PlantListItem plant={plant} key={plant.id} setItems={setItems} />
                    ))
                }
            </tbody>
        </table>
        </>
    );
}

