import { Plant } from "@/types/plantType";
import Image from "next/image";
import PlantForm from "./plantForm";
import { supabase } from "@/utils/supabaseClient";
import { Button } from "@nextui-org/react";

export default function PlantListItem({ plant, setItems }: { plant: Plant, setItems: Function }){

    async function deletePlant({ plantId }: {plantId: string }) {
        await supabase.from('species').delete().eq('id', plantId);
        const { data } = await supabase.from('species').select('*').order('created_at', { ascending: false });
        setItems(data);
    }
    
    return (
        <tr key={plant.id}>
            <td className="border px-4 py-4">{plant.id}</td>
            <td className="border px-4 py-4">{plant.name}</td>
            <td className="border px-8 py-4">{plant.slug}</td>
            <td className="border px-8 py-4">{plant.description}</td>
            <td className="border px-8 py-4">{plant.zone}</td>
            <td className="border px-8 py-4">
                <Image
                    src={plant.seed_image}
                    alt="picture"
                    width="200"
                    height="200"
                />
            </td>
            <td className="border px-8 py-4">{(new Date(plant.created_at)).toLocaleString()}</td>
            <td className="border px-8 py-4">
                {" "}
                <PlantForm plant={plant} setItems={setItems} buttonText="Edit" />
                <Button color="warning" onPress={() => deletePlant({ plantId : plant.id })}
                >
                    Delete
                </Button>
            </td>
        </tr>
    );
} 