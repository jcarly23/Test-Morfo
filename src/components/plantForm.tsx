import { Plant } from "@/types/plantType";
import { supabase } from "@/utils/supabaseClient";
import {Modal, ModalContent, ModalHeader, ModalBody, ModalFooter, Button, Input, useDisclosure} from "@nextui-org/react";
import { useState } from "react";

export default function PlantForm({plant, setItems, buttonText} : {plant : Plant, setItems: Function, buttonText: string}) {
    const { isOpen, onOpen, onOpenChange } = useDisclosure();

    const [plantName, setPlantName] = useState(plant.name);
    const [plantSlug, setPlantSlug] = useState(plant.slug);
    const [plantDescription, setPlantDescription] = useState(plant.description);
    const [plantZone, setPlantZone] = useState(plant.zone);
    const [file, setFile] = useState([]);
    
    async function save({ plant }: { plant: Plant }) {
        if (file && file.name) {
            const filename = `${crypto.randomUUID()}-${file.name}`;
            const { data, error } = await supabase.storage
                .from("species")
                .upload(filename, file, {
                    cacheControl: "3600",
                    upsert: false,
                });

            console.log(data);
            console.log(error);
            const filepath = data.path;

            const response = supabase
                .storage
                .from('species')
                .getPublicUrl(`${filepath}`);
            console.log(response);
            plant.seed_image = response.data.publicUrl;
        }
        if (plant.id) {
            await supabase.from('species').update(plant).eq('id', plant.id);
        }
        else {
            plant.id = crypto.randomUUID();
            await supabase.from('species').insert(plant);
        }
        const r = await supabase.from('species').select('*').order('created_at', { ascending: false });
        setItems(r.data);
        setPlantName('');
        setPlantSlug('');
        setPlantDescription('');
        setPlantZone('');
    }

    const handleFileSelected = (e) => {
        setFile(e.target.files[0]);
    };

    return (
        <>
            <Button onPress={onOpen} color="primary">{buttonText}</Button>
            <Modal 
                isOpen={isOpen} 
                onOpenChange={onOpenChange}
                placement="top-center"
            >
                <ModalContent>
                {(onClose) => (
                    <>
                    <ModalHeader className="flex flex-col gap-1">{ }</ModalHeader>
                    <ModalBody>
                        <Input
                            autoFocus
                            label="Name"
                            variant="bordered"
                            value={plantName} 
                            onChange={e => { setPlantName(e.currentTarget.value); }}
                        />
                        <Input
                            label="Slug"
                            variant="bordered"
                            value={plantSlug} 
                            onChange={e => { setPlantSlug(e.currentTarget.value); }}
                        />
                        <Input
                            label="Description"
                            variant="bordered"
                            value={plantDescription} 
                            onChange={e => { setPlantDescription(e.currentTarget.value); }}
                        />
                        <Input
                            label="Zone"
                            variant="bordered"
                            value={plantZone} 
                            onChange={e => { setPlantZone(e.currentTarget.value); }}
                        />
                        <Input
                            label="Image"
                            type="file"
                            variant="bordered"
                            onChange={handleFileSelected}
                        />
                    </ModalBody>
                    <ModalFooter>
                        <Button color="primary" onPress={() => {
                            save({
                                plant: {
                                    id: plant.id,
                                    name: plantName,
                                    slug: plantSlug,
                                    description: plantDescription,
                                    zone: plantZone,
                                    seed_image: plant.seed_image
                                }
                            });
                            onClose();
                        }}>
                            Save
                        </Button>
                    </ModalFooter>
                    </>
                )}
                </ModalContent>
            </Modal>
        </>
    );
}

