import { supabase } from "@/utils/supabaseClient";
import { Plant } from "@/types/plantType";
import PlantList from "@/components/plantList";

export async function getStaticProps() {
  const { data } = await supabase.from('species').select('*').order('created_at', { ascending: false });
  return {
    props: {
      plants: data,
    },
  };
}

export default function Home({ plants }: { plants: Plant[] }) {
  return (
    <main className="flex flex-col items-center justify-center w-full flex-1 px-20 text-center">
          <PlantList plants={plants} />
    </main>
  );
}

