export type Plant = {
    id: string | null;
    name: string;
    slug: string;
    description: string;
    zone: string;
    seed_image: string;
    created_at: string;
  };